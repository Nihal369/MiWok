package com.lmntrx.android.nihal.miwok;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class Phrases extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phrases);
        ListView listView=(ListView)findViewById(R.id.phrasesList);

        ArrayList<Word> arrayList=new ArrayList<Word>();
        arrayList.add(new Word("Where are you going?","Minto Wuksus",R.mipmap.ic_launcher,"phrases"));
        arrayList.add(new Word("What is your name","Tinne oyaase'ne",R.mipmap.ic_launcher,"phrases"));
        arrayList.add(new Word("My name is","Ayaaset",R.mipmap.ic_launcher,"phrases"));
        arrayList.add(new Word("How are you feeling?","Michaksas?",R.mipmap.ic_launcher,"phrases"));
        arrayList.add(new Word("I’m feeling good.","Kuchi achit",R.mipmap.ic_launcher,"phrases"));
        arrayList.add(new Word("Are you coming?","Eenes'aa?",R.mipmap.ic_launcher,"phrases"));
        arrayList.add(new Word("Yes, I’m coming.","Hee aanam",R.mipmap.ic_launcher,"phrases"));
        arrayList.add(new Word("I'm Coming","Aanam",R.mipmap.ic_launcher,"phrases"));
        arrayList.add(new Word("Let's go","Yoowutis",R.mipmap.ic_launcher,"phrases"));
        arrayList.add(new Word("Come here","Anninem",R.mipmap.ic_launcher,"phrases"));

        WordAdapter wordAdapter=new WordAdapter(this,arrayList);

        listView.setAdapter(wordAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if(position==0)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.phrase_where_are_you_going);
                    mediaPlayer.start();
                }

                else if(position==1)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.phrase_what_is_your_name);
                    mediaPlayer.start();
                }

                else if(position==2)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.phrase_my_name_is);
                    mediaPlayer.start();
                }

                else if(position==3)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.phrase_how_are_you_feeling);
                    mediaPlayer.start();
                }

                else if(position==4)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.phrase_im_feeling_good);
                    mediaPlayer.start();
                }

                else if(position==5)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.phrase_are_you_coming);
                    mediaPlayer.start();
                }

                else if(position==6)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.phrase_yes_im_coming);
                    mediaPlayer.start();
                }

                else if(position==7)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.phrase_im_coming);
                    mediaPlayer.start();
                }

                else if(position==8)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.phrase_lets_go);
                    mediaPlayer.start();
                }

                else if(position==9)
                {
                    MediaPlayer mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.phrase_come_here);
                    mediaPlayer.start();
                }

            }
        });
    }
}
